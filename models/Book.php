<?php

namespace app\models;

use Yii;
use yii\behaviors;
use yii\behaviors\SluggableBehavior;
// To use the Yii2 Query Builder for making SELECT queries
use yii\db\Query;
use yii\db\mssql\PDO;

/**
 * This is the model class for table "book".
 *
 * @property integer $idbook
 * @property string $title
 * @property string $description
 * @property string $date_entered
 * @property string $date_updated
 * @property integer $working
 * @property string $asin
 * @property string $isbn
 * @property string $isbn13
 * @property string $language
 * @property string $author
 * @property string $publisher
 * @property string $amazon_url
 * @property string $amazon_price_eur
 * @property string $amazon_cover_url
 */
class Book extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'book';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['description'], 'string'],
            [['date_entered', 'date_updated'], 'safe'],
            [['working'], 'integer'],
            [['amazon_cover_url'], 'required'],
            [['title'], 'string', 'max' => 100],
            [['asin', 'isbn', 'isbn13', 'language', 'author', 'publisher', 'amazon_price_eur'], 'string', 'max' => 45],
            [['amazon_url'], 'string', 'max' => 512],
            [['amazon_cover_url'], 'string', 'max' => 512],
            [['asin'], 'required'],
            [['asin'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idbook' => 'Idbook',
            'title' => 'Título',
            'description' => 'Descripción',
            'date_entered' => 'Fecha de entrada',
            'date_updated' => 'Fecha de modificación',
            'working' => 'Activo',
            'asin' => 'Asin',
            'isbn' => 'Isbn',
            'isbn13' => 'Isbn13',
            'language' => 'Idioma',
            'author' => 'Autor',
            'publisher' => 'Editorial',
            'amazon_url' => 'Amazon - Url',
            'amazon_price_eur' => 'Amazon - Precio en €',
            'amazon_cover_url' => 'Amazon - URL a la portada',
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            // 'slugAttribute' => 'slug',
            ],
        ];
    }


    
    /*
     * @action: favorite, pending, finished
     */
    public function userList($bookId, $userId, $action) {
        /* To prevent SQL injection attacks through DAO, you can either 
         * pass values in arrays, or use bound parameters 
         * Here I'm using bound parameters
         */
        $q = 'INSERT INTO user_book_'.$action.' (user_id, book_id, '.$action.') '
                . 'VALUES (:user_id, :book_id, 1) '
                . 'ON DUPLICATE KEY UPDATE '
                . $action.' = NOT '.$action;
        
        $cmd = Yii::$app->db->createCommand($q);
        $cmd->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $cmd->bindValue(':book_id', $bookId, PDO::PARAM_INT);
        $cmd->execute();
    }

}
