<?php

namespace app\controllers;

use Yii;
use app\models\Book;
use app\models\BookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
//use app\components\BaseController;
use yii\web\Response;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller {

    public $jsFile;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'favorites', 'pending', 'finished'],
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['moderador'], // Only the site administrators can do that
                    ],
                    [
                        'actions' => ['favorites', 'pending', 'finished'],
                        'allow' => true,
                        'roles' => ['@'], // All loged in users can favorite a book
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'ajax' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new BookSearch([ 'working' => 1,]); // We will display only the working books
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Book::find()
                    ->where(['working' => 1])
                    ->orderBy(['date_entered' => SORT_DESC])
            , 'pagination' => ['pageSize' => 12]
        ]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Book models WITH A GRIDVIEW.
     * @return mixed
     */
    public function actionSearch() {
        $searchModel = new BookSearch([ 'working' => 1,]); // We will display only the working books
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=4;

        return $this->render('search', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /* -------- BOOK VIEW ROUTING --------
     * To avoid using the id in the book view route (as in /libro/1/el-club-de-la-lucha)
     * we need to:
     * 1. Change the route rule (found in /config/web.php) 
     *      to: 'libro/<id:\d+>/<slug>' => 'book/view',
     * 
     * 2. Disable the actionView($id) method
     * 3. Enable actionView($slug) and findModelBySlug($slug)
      
     * I choosed to use the ID in the route for SEO pourposes:
     * If the title is changed later on and we are using just the slug, the link  
     *      would become broken and we'd lose search engine traffic
     * Now the slug doesn't affect the routing, it just need to have any text
     *      F.e: /libro/1/a would work as well as /libro/1/el-club-de-la-lucha
     * 
     */
//    public function actionView($slug) {
//        return $this->render('view', [
//                    'model' => $this->findModelBySlug($slug),
//        ]);
//    }
//    protected function findModelBySlug($slug) {
//        if (($model = Post::findOne(['slug' => $slug])) !== null) {
//            return $model;
//        } else {
//            throw new NotFoundHttpException();
//        }
//    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Book();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = $model->amazon_cover_url;
            $img = \Yii::getAlias('@webroot').'/img/covers/'.$model->idbook.'.jpg';
            file_put_contents($img, file_get_contents($url));
            return $this->redirect(['view', 'id' => $model->idbook]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $url = $model->amazon_cover_url;
            $img = \Yii::getAlias('@webroot').'/img/covers/'.$model->idbook.'.jpg';
            file_put_contents($img, file_get_contents($url));
            return $this->redirect(['view', 'id' => $model->idbook]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Book model.
     * But instead of deleting the row, we will set the working attribute to 0
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
//        $this->findModel($id)->delete(); // That method deletes the row
        $model = $this->findModel($id);
        $model->working = 0; // This way we are preserving the data
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {

        if (($model = Book::findOne($id)) !== null) {
            return $model;
//        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionFavorites() {

        $userId = Yii::$app->user->identity->id;

        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Book::find()
                    ->leftJoin('user_book_favorite', '`book`.`idbook` = `user_book_favorite`.`book_id`')
                    ->where(['user_book_favorite.user_id' => $userId,
                        'user_book_favorite.favorite' => 1,
                        'book.working' => 1])
                    ->orderBy(['user_book_favorite.timestamp_modified' => SORT_DESC])
            , 'pagination' => ['pageSize' => 12]
        ]);

        return $this->render('favorites', ['dataProvider' => $dataProvider,]);
    }

    public function actionPending() {
        $userId = Yii::$app->user->identity->id;

        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Book::find()
                    ->leftJoin('user_book_pending', '`book`.`idbook` = `user_book_pending`.`book_id`')
                    ->where(['user_book_pending.user_id' => $userId,
                        'user_book_pending.pending' => 1,
                        'book.working' => 1])
                    ->orderBy(['user_book_pending.timestamp_modified' => SORT_DESC])
            , 'pagination' => ['pageSize' => 12]
        ]);

        return $this->render('pending', ['dataProvider' => $dataProvider,]);
    }

    public function actionFinished() {
        $userId = Yii::$app->user->identity->id;

        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Book::find()
                    ->leftJoin('user_book_finished', '`book`.`idbook` = `user_book_finished`.`book_id`')
                    ->where(['user_book_finished.user_id' => $userId,
                        'user_book_finished.finished' => 1,
                        'book.working' => 1])
                    ->orderBy(['user_book_finished.timestamp_modified' => SORT_DESC])
            , 'pagination' => ['pageSize' => 12]
        ]);

        return $this->render('finished', ['dataProvider' => $dataProvider,]);
    }

    public function actionAjax() {
        if (Yii::$app->user->isGuest) {
            return error;
        }
        $userId = Yii::$app->user->identity->id;

        $bookId = Yii::$app->request->post('id');
        !$model = $this->findModel($bookId);
        if (!$model) {
            return error;
        }

        $ajaxAction = Yii::$app->request->post('action');
        switch ($ajaxAction) {
            case 'favorite':
            case 'pending':
            case 'finished':
                break;
            default:
                return error;
        }


        $model->userList($bookId, $userId, $ajaxAction);
    }
    

}
