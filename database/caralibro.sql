-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2016 a las 12:56:18
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `caralibro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('moderador', '1', 1465206407),
('moderador', '2', 1465205720);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('moderador', 1, 'Puede hacer CRUD en los libros', NULL, NULL, 1465205229, 1465821995);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `book`
--

CREATE TABLE IF NOT EXISTS `book` (
`idbook` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `date_entered` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `working` tinyint(4) DEFAULT '1',
  `asin` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isbn` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isbn13` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publisher` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amazon_url` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amazon_price_eur` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amazon_cover_url` varchar(512) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `book`
--

INSERT INTO `book` (`idbook`, `title`, `slug`, `description`, `date_entered`, `date_updated`, `working`, `asin`, `isbn`, `isbn13`, `language`, `author`, `publisher`, `amazon_url`, `amazon_price_eur`, `amazon_cover_url`) VALUES
(1, 'El club de la lucha', 'el-club-de-la-lucha', 'La primera regla del club de la lucha es no hablar del club de la lucha. Cada fin de semana, un puñado de jóvenes oficinistas se quita los zapatos y las camisas y pelean entre sí hasta la extenuación. Los lunes vuelven a sus despachos con los ojos amoratados y un embriagador sentimiento de omnipotencia. Pero estas reuniones son solo el comienzo del plan con el que Tyler Durden, proyeccionista, camarero y oscuro genio anárquico, aspira a vengarse de una sociedad paralizada por el consumismo exacerbado.\r\n', '2016-05-22 16:24:40', '2016-06-15 08:34:18', 1, '8499088171', '8499088171', '978-8499088174', 'spanish', 'Chuck Palahniuk', 'Debolsillo', 'https://www.amazon.es/gp/product/8499088171/', '8.5', 'https://images-na.ssl-images-amazon.com/images/I/51UTlvFsvIL._SX327_BO1,204,203,200_.jpg'),
(2, 'Mil Novecientos Ochenta Y Cuatro', 'mil-novecientos-ochenta-y-cuatro', 'En el año 1984 Londres es una ciudad lúgubre en la que la Policía del Pensamiento controla de forma asfixiante la vida de los ciudadanos. Winston Smith es un peón de este engranaje perverso y su cometido es reescribir la historia para adaptarla a lo que el Partido considera la versión oficial de los hechos. Hasta que decide replantearse la verdad del sistema que los gobierna y somete.\r\n\r\nLa crítica ha dicho...\r\n«Aquí ya no estamos solo ante lo que habitualmente reconocemos como "literatura" e identificamos con la buena escritura. Aquí estamos, repito, ante energía visionaria. Y no todas las visiones se refieren al futuro, o al Más Allá.»\r\nUmberto Eco\r\n\r\n«Entre mis libros favoritos, lo leo una y otra vez.»\r\nMargaret Atwood\r\n\r\n«No es difícil pensar que Orwell, en 1984, estuviera imaginando un futuro para la generación de su hijo, un mundo del que deseaba prevenirles.»\r\nThomas Pynchon\r\n\r\n«La libertad es una obligación tan dolorosa que siempre habrá quien prefiera rendirse. La virtud de libros como 1984 es su capacidad para recordarnos que la libertad de los seres humanos responsables no es igual a la de los animales.»\r\nAnthony Burgess\r\n\r\n«Desde El proceso de Kafka ninguna obra fantástica ha alcanzado el horror lógico de 1984.»\r\nArthur Koestler\r\n\r\n«Un libro magnífico y profundamente interesante.»\r\nAldous Huxley', '2016-05-22 16:26:05', '2016-06-14 20:25:12', 1, '8499890946', '8499890946', '978-8499890944', 'spanish', 'George Orwell', 'Debolsillo', 'https://www.amazon.es/gp/product/8499890946/', '7.55', 'https://images-na.ssl-images-amazon.com/images/I/51NZ7zA77wL._SX326_BO1,204,203,200_.jpg'),
(3, 'Rebelión En La Granja', 'rebelion-en-la-granja', 'Esta sátira de la Revolución rusa y el triunfo del estalinismo, escrita en 1945, se ha convertido por derechos propio en un hito de la cultura contemporánea y en uno de los libros más mordaces de todos los tiempos. Ante el auge de los animales de la Granja Solariega, pronto detectamos las semillas de totalitarismo en una organización aparentemente ideal; y en nuestros líderes más carismáticos, la sombra de los opresores más crueles.\r\n\r\nLa crítica dijo...\r\n«Una obra literaria perfecta.»\r\nT.S. Elliot\r\n\r\n«George Orwell es un hombre con extraordinario sentido moral y con un enorme respeto por la justicia y la verdad.»\r\nEvelyn Waugh\r\n\r\n«El libro que todo el mundo, toda la gente de a pie, debería leer. No ha perdido un ápice de lucidez en cincuenta años.»\r\nRuth Rendell\r\n\r\n«Casi antes que nadie él comprendió que la corrupción de las palabras es un síntoma y a la vez la causa de la corrupción del pensamiento.»\r\nAntonio Muñoz Molina\r\n\r\n«Desde Los viajes de Gulliver no se ha escrito una parábola tan profunda, mordiente y satírica como Rebelión en la granja.»\r\nArthur Koestler\r\n\r\n«Un intelectual radicalmente independiente cuya obra es de una claridad moral insobornable.»\r\nGuillermo Altares, El País', '2016-05-22 16:27:05', '2016-06-14 20:25:39', 1, '8499890954', '8499890954', '978-8499890951', 'spanish', 'George Orwell', 'Debolsillo', 'https://www.amazon.es/gp/product/8499890954/', '6.6', 'https://images-na.ssl-images-amazon.com/images/I/515iv68CIwL._SX328_BO1,204,203,200_.jpg'),
(4, 'La naranja mecánica', 'la-naranja-mecanica', 'La historia del nadsat-adolescente Alex y sus tres drugos-amigos en un mundo de crueldad y destrucción. Alex tiene, según Burgess, "los principales atributos humanos; amor a la agresión, amor al lenguaje, amor a la belleza. Pero es joven y no ha entendido aún la verdadera importancia de la libertad, la que disfruta de un modo tan violento. En cierto sentido vive en el Edén, y sólo cuando cae (como en verdad le ocurre, desde una ventana) parece capaz de llegar a transformase en un verdadero ser humano".', '2016-06-14 19:56:57', '2016-06-14 19:57:47', 1, '8445078828', '8445078828', '978-8445078822', 'spanish', 'Anthony Burgess', 'Minotauro', 'https://www.amazon.es/gp/product/8445078828/', '7.55', 'https://images-na.ssl-images-amazon.com/images/I/41efWAIog-L._SX330_BO1,204,203,200_.jpg'),
(5, 'Trainspotting', 'trainspotting', 'Muy pocas veces alguien se atrevió a recomendar tan fervientemente una novela. «Merece vender más ejemplares que la Biblia», afirmó Rebel Inc., una insolente revista literaria escocesa. De inmediato celebrada por los críticos más estrictos pero leída también por aquellos que raramente se acercan a los libros, "Trainspotting" se convirtió en uno de los acontecimientos literarios y también extraliterarios de la última década. Fue rápidamente adaptada al teatro y luego llevada a la pantalla por Danny Boyle, uno de los jóvenes prodigio del cine inglés. Sus protagonistas son un grupo de jóvenes desesperadamente realistas, ni se les ocurre pensar en el futuro: saben que nada o casi nada va a cambiar, habitantes del otro Edimburgo, el que no aparece en los famosos festivales, capital europea del sida y paraíso de la desocupación, la miseria y la prostitución, embarcados en una peripecia vital cuyo combustible es la droga, «el elixir que les da la vida, y se la quita». Welsh escribe en el áspero, colorido, vigoroso lenguaje de las calles. Y entre pico y pico, entre borracheras y fútbol, sexo y rock and roll, la negra picaresca, la épica astrosa de los que nacieron en el lado duro de la vida, de los que no tienen otra salida que escapar, o amortiguar el dolor de existir con lo primero que caiga en sus manos.', '2016-06-14 19:59:33', NULL, 1, '843396643X', '843396643X', '978-8433966438', 'spanish', 'Irvine Welsh', 'Anagrama', 'https://www.amazon.es/gp/product/843396643X/r', '10.36', 'https://images-na.ssl-images-amazon.com/images/I/41LZsNjXagL._SX326_BO1,204,203,200_.jpg'),
(6, 'Asfixia', 'asfixia', 'Victor Mancini, un estudiante de medicina fracasado, ha diseñado un complejo plan para cubrir los gastos de la atención médica de su madre. Fingir que se asfixia con un pedazo de comida en un restaurante para que la persona que acuda a «salvarlo» se sienta responsable de él durante el resto de su vida. Y le pase un cheque. Si se practica en varios centenares de restaurantes, la lluvia de dinero está garantizada.\r\n\r\nEntre una actuación y otra en los restaurantes, Victor también se gana la vida representando el papel de un campesino miserable en un parque temático dedicado a la América colonial del siglo XVIII, acude a un grupo de terapia para adictos al sexo (la mejor manera de conocer mujeres) y visita a su madre, cuya vena anárquica hizo de la infancia de Victor una auténtica locura.\r\n\r\nUn antihéroe en tiempos de insania, y una novela que confirma a Palahniuk, autor de El club de la lucha, como una de las voces más frescas y desopilantes de la narrativa norteamericana.', '2016-06-14 20:01:52', '2016-06-16 10:52:22', 1, '8499088988', '8499088988', '978-8499088983', 'spanish', 'Chuck Palahniuk', 'Debolsillo', 'https://www.amazon.es/gp/product/8499088988/', '8.50', 'https://images-na.ssl-images-amazon.com/images/I/31wXApMY8cL._SX327_BO1,204,203,200_.jpg'),
(7, 'Un mundo feliz', 'un-mundo-feliz', 'Un mundo feliz es un clásico de la literatura del siglo XX, una sombría metáfora sobre el futuro.\r\n\r\nLa novela describe un mundo en el que finalmente se han cumplido los peores vaticinios: triunfan los dioses del consumo y la comodidad, y el orbe se organiza en diez zonas en apariencia seguras y estables. Sin embargo, este mundo ha sacrificado valores humanos esenciales, y sus habitantes son procreados in vitro a imagen y semejanza de una cadena de montaje.\r\n\r\nLa crítica ha dicho...\r\n\r\n«Aldous Huxley fue un hombre extraordinariamente profético, no hay otro novelista en el siglo XX que haya escrito una guía más sagaz del futuro.»\r\n\r\nJ.G. Ballard', '2016-06-14 20:03:30', NULL, 1, '8497594258', '8497594258', '978-8497594257', 'spanish', 'Aldous Huxley', 'Debolsillo', 'https://www.amazon.es/gp/product/8497594258/', '9.45', 'https://images-na.ssl-images-amazon.com/images/I/51Ti61BfZnL._SX324_BO1,204,203,200_.jpg'),
(8, 'El Arte de la Guerra', 'el-arte-de-la-guerra', 'El Arte de la Guerra es el mejor libro de estrategia de todos los tiempos. Inspiró a Napoleón, Maquiavelo, Mao Tse Tung y muchas más figuras históricas. Este libro de dos quinientos mil años de antigüedad, es uno de los más importantes textos clásicos chinos, en el que, a pesar del tiempo transcurrido, ninguna de sus máximas ha quedado anticuada, ni hay un solo consejo que hoy no sea útil. Pero la obra del general Sun Tzu no es únicamente un libro de práctica militar, sino un tratado que enseña la estrategia suprema de aplicar con sabiduría el conocimiento de la naturaleza humana en los momentos de confrontación. No es, por tanto, un libro sobre la guerra; es una obra para comprender las raíces de un conflicto y buscar una solución. “la mejor victoria es vencer sin combatir”, nos dice Sun Tzu, “y ésa es la distinción entre el hombre prudente y el ignorante”.', '2016-06-14 20:06:03', NULL, 1, '148407291X', '148407291X', '978-1484072912', 'spanish', 'Sun Tzu', 'Createspace', 'https://www.amazon.es/gp/product/148407291X/', '3.86', 'https://images-na.ssl-images-amazon.com/images/I/61Aw6uBcdkL._SX331_BO1,204,203,200_.jpg'),
(9, 'Fahrenheit 451', 'fahrenheit-451', 'Un clásico de la ciencia ficción.\r\n\r\nFahrenheit 451 cuenta la historia de un sombrío y horroroso futuro. Montag, el protagonista, pertenece a una extraña brigada de bomberos cuya misión, paradójicamente, no es la de sofocar incendios, sino la de provocarlos para quemar libros. Porque en el país de Montag está terminantemente prohibido leer. Porque leer obliga a pensar, y en el país de Montag está prohibido pensar. Porque leer impide ser ingenuamente feliz, y en el país de Montag hay que ser feliz a la fuerza...\r\n\r\nReseña:\r\n«Bradbury es heredero de la vasta imaginación del maestro Poe.»\r\nJorge Luis Borges\r\n\r\n«Incomparable. Un maestro en el delicado equilibrio entre ciencia y fantasía.»\r\nC. S. Lewis\r\n\r\n«La fuerza y la potencia de su original imaginación llenan de júbilo... Un talento mayor e insólito.»\r\nChristopher Isherwood\r\n\r\n«De entre todos los infiernos del conformismo, Fahrenheit 451 ofrece el retrato más convincente.»\r\nKingsley Amis', '2016-06-14 20:10:44', NULL, 1, '8490321477', '8490321477', '978-8490321478', 'spanish', 'Ray Bradbury', 'Debolsillo', 'https://www.amazon.es/gp/product/8490321477/', '8.5', 'https://images-na.ssl-images-amazon.com/images/I/41J4bXU2RcL._SX324_BO1,204,203,200_.jpg'),
(10, 'El viejo y el mar', 'el-viejo-y-el-mar', '«Su mejor obra. El tiempo demostrará que es la mejor que cualquiera de nosotros haya escrito, y con eso me refiero a sus coetáneos y a los míos.»\r\n\r\nWilliam Faulkner\r\n\r\nCon un lenguaje de gran fuerza y sencillez, El viejo y el mar narra la historia de un viejo pescador cubano a quien la suerte parece haber abandonado, y del desafío mayor al que se enfrenta: la batalla despiadada y sin tregua con un pez gigantesco en las aguas del golfo.\r\n\r\nEscrito en 1952 por encargo de la revista Life, este relato lo confirmó como uno de los escritores más significativos del siglo XX, obteniendo el Premio Pulitzer en 1953 y allanando su carrera hacia el Premio Nobel de Literatura, que recibió en 1954.\r\n\r\nReseñas:\r\n\r\n«El novelista norteamericano más importante del siglo XX.»\r\n\r\nWilliam Faulkner\r\n\r\n«Al igual que Rousseau, Hemingway nos ha señalado como prueba del valor humano la capacidad de medirse, de lograr, de fracasar.»\r\n\r\nItalo Calvino\r\n\r\n«Tal vez en ningún otro escritor moderno la proeza física, el coraje, la fuerza bruta y el espíritu de destrucción alcanzan una dignidad parecida.»\r\n\r\nMario Vargas Llosa\r\n\r\n«En sus páginas descubrí que la vida alcanzaba verdadera categoría si la asumíamos decididos a llegar al final de las empresas, sin que importase si estas eran o no trascendentes para los demás.»\r\n\r\nLuis Sepúlveda', '2016-06-14 20:12:28', '2016-06-14 20:34:24', 1, '8499089984', '8499089984', '978-8499089980', 'spanish', 'Ernest Hemingway', 'Debolsillo', 'https://www.amazon.es/viejo-mar-edición-escolar-CONTEMPORANEA/dp/8499089984/', '8.5', 'https://images-na.ssl-images-amazon.com/images/I/41-Si0BRY5L._SX327_BO1,204,203,200_.jpg'),
(11, 'Por quién doblan las campanas', 'por-quien-doblan-las-campanas', 'Por quién doblan las campanas es una de las novelas más populares de Hemingway. Ambientada en la guerra civil española, la obra es una bella historia de amor y muerte que se ha convertido en un clásico de nuestro tiempo.\r\n\r\nEn los tupidos bosques de pinos de una región montañosa española, un grupo de milicianos se dispone a volar un puente esencial para la ofensiva republicana. La acción cortará las comunicaciones por carretera y evitará el contraataque de los sublevados.\r\n\r\nRobert Jordan, un joven voluntario de las Brigadas Internacionales, es el dinamitero experto que ha venido a España para llevar a cabo esta misión. En las montañas descubrirá los peligros y la intensa camaradería de la guerra. Y descubrirá también a María, una joven rescatada por los milicianos de manos de las fuerzas sublevadas de Franco, de la cual se enamorará enseguida.\r\n\r\nReseña:\r\n\r\n«La novela que le dio a Hemingway lo mejor y lo peor que puede recibir alguien con mentalidad de atleta: un triunfo insuperable.»\r\nJuan Villoro', '2016-06-14 20:13:54', NULL, 1, '8497935020', '8497935020', '978-8497935029', 'spanish', 'Ernest Hemingway', 'Debolsillo', 'https://www.amazon.es/Por-quién-doblan-campanas-CONTEMPORANEA/dp/8497935020/', '9.45', 'https://images-na.ssl-images-amazon.com/images/I/51HIJhEgb8L._SX327_BO1,204,203,200_.jpg'),
(12, 'El Club De La Lucha 2', 'el-club-de-la-lucha-2', 'Veinte años después de la primera publicación de El club de la lucha, llega su esperada secuela... directamente como novela gráfica. Chuck Palahniuk ha elaborado un guion a la altura de sus mejores trabajos y Cameron Stewart lo ilustra con un espectacular desempeño.\r\n\r\nDiez años después del nacimiento del proyecto Estragos, Sebastian (así se llama el narrador de la historia) lleva una vida mundana. Tiene un trabajo insignificante en una empresa contratista militar llamada Levantarse o morir, se ha casado con Marla y tienen un hijo. Gracias a la medicación se ha mantenido a raya durante todo este tiempo, pero no por mucho más. Tyler Durden va a regresar más rebelde y anárquico que nunca.\r\n\r\nCríticas:\r\n«Si Tyler Durden tenía que regresar, no había mejor momento que el presente. El club de la lucha 2 es un cómic que completa e incluso mejora su obra de origen, con un Tyler Durden cuya sincera asunción del nihilismo nos reabre de par en par las puertas de un mundo conocido donde reinan el cinismo, la violencia y la anarquía. "Tyler vive" y no podríamos ser más felices ante la explosión de caos que se acerca.»\r\nNewsarama\r\n\r\n«Tan conmovedoras como hilarantes, las fábulas desfiguradas de Palahniuk retratan el retorcido espíritu de nuestra época y se suman, con una voz propia, extrañamente sugestiva y poética, al canon contemporáneo. En un presente hiperactivo, obsesionado con el terrorismo, internet, la guerra y la crisis mundial, es normal que necesitemos más que nunca que Tyler vuelva.»\r\nThe Atlantic\r\n\r\n«Palahniuk ha escrito una valiosísima secuela a su historia más popular.»\r\nNerdist\r\n\r\n«Damas y caballeros, vamos a incumplir las dos primeras reglas del club de la lucha.»\r\nCraveOnline\r\n\r\n«Una obra extraordinaria. No te la pierdas por nada del mundo.»\r\nComic Vine\r\n\r\n«El club de la lucha 2 es un cómic de lectura endiablada que sin duda redefinirá la industria, por el carácter del peso pesado proveniente de otro medio que se mete en este tipo de faenas. No se pueden tener más ganas de leer.»\r\nPaste\r\n\r\n«El humor es puro Palahniuk: alegremente incorrecto y rebosante de metáforas.»\r\nING\r\n\r\n«Bello e inteligente.»\r\nComicsAlliance\r\n\r\n«Excelente.»\r\nThe Beat', '2016-06-14 20:15:29', NULL, 1, '8416195889', '8416195889', '978-8416195886', 'spanish', 'Chuck Palahniuk', 'Reservoir Books', 'https://www.amazon.es/gp/product/8416195889/', '20.81', 'https://images-na.ssl-images-amazon.com/images/I/51URmNxwzKL._SX323_BO1,204,203,200_.jpg'),
(13, 'El Padrino', 'el-padrino', 'La publicación de El Padrino en 1969 supuso una convulsión en el mundo literario. Por primera vez, la Mafia protagonizaba una novela y era retratada desde dentro con acierto y verosimilitud. Mario Puzo la presentaba no como una mera asociación de facinerosos, sino como una compleja sociedad con una cultura propia y una jerarquía aceptada incluso más allá de los círculos de delincuencia. El Padrino narra la historia de un hombre: Vito Corleone, el capo más respetado de Nueva York. Déspota benevolente, implacable con sus rivales, inteligente, astuto y fiel a los principios del honor y la amistad, Don Corleone dirige un emporio que abarca el fraude y la extorsión, los juegos de azar y el control de los sindicatos. La vida y negocios de Don Corleone, así como los de su hijo y heredero Michael, conforman el eje de esta magistral obra. Con El Padrino, Mario Puzo partió de la realidad y consiguió crear un género. La Mafia pasó a ser tema central de centenares de novelas y películas, aunque ninguna de ellas ha alcanzado el nivel de la obra que las inspiró.', '2016-06-14 20:17:40', NULL, 1, '8498723523', '8498723523', '978-8498723526', 'spanish', 'Mario Puzo', 'Zeta Bolsillo', 'https://www.amazon.es/PADRINO-BEST-SELLER-ZETA-BOLSILLO/dp/8498723523/', '9.5', 'https://images-na.ssl-images-amazon.com/images/I/31jGmpCyToL._BO1,204,203,200_.jpg'),
(14, 'El Siciliano', 'el-siciliano', 'El Siciliano narra la historia de un personaje real (el bandolero Salvatore Giuliano) desde la perspectiva de un personaje de ficción: Michael Corleone, hijo de Don Vito, quien lo conoce durante su exilio en Palermo. Giuliano fue una leyenda viva; tras enfrentarse en su juventud a los carabinieri, se refugió en las montañas y empezó a luchar por los menesterosos sicilianos que se encontraban bajo el yugo de la Mafia y la corrupción del Gobierno de Roma.', '2016-06-14 20:19:11', NULL, 1, '8490700206', '8490700206', '978-8490700204', 'spanish', 'Mario Puzo', 'Zeta Bolsillo', 'https://www.amazon.es/El-Siciliano-BOLSILLO-Mario-Puzo/dp/8490700206/', '4.75', 'https://images-na.ssl-images-amazon.com/images/I/41DWu6bP3YL._SX315_BO1,204,203,200_.jpg'),
(15, 'El Último Don', 'el-ultimo-don', 'Un año después de cometer el acto más salvaje de su vida, Domenico Clericuzio, capo de la última gran familia de la Mafia, toma una determinación: la generación de sus nietos vivirá ajena al mundo del crimen.\r\n \r\nEl Don controla negocios en Las Vegas y Hollywood que le permitirían legitimar su fortuna, pero hay una cuestión que no puede ignorar: lo único que los Clericuzio saben hacer bien es matar.', '2016-06-14 20:20:13', NULL, 1, '8498729025', '8498729025', '978-8498729023', 'spanish', 'Mario Puzo', 'Zeta Bolsillo', 'https://www.amazon.es/El-Último-Don-B-BOLSILLO/dp/8498729025/r', '4.75', 'https://images-na.ssl-images-amazon.com/images/I/41BQWhdWVWL._SX311_BO1,204,203,200_.jpg'),
(16, 'La Familia Corleone', 'la-familia-corleone', 'Una extraordinaria novela de tradiciones y violencia, de lealtad y traición que apelará a los miles de lectores de El Padrino.\r\nNueva York, 1933. La ciudad y la nación están sumidas en la Gran Depresión. Las familias del crimen han prosperado durante los últimos años, pero al acercarse el fin de la Prohibición, se aproxima una guerra que determinará cuales organizaciones sobrevivirán y cuales verán su final.\r\n\r\nPara Vito Corleone nada es más importante que el futuro de su familia.Mientras sus tres hijos menores asisten al colegio, desconocedores de la verdadera ocupación de su padre, y su hijo adoptivo Toma Hagen estudia la Universidad, su gran preocupación es Sonny, su primogénito.\r\n\r\nVito quiere que Sonny se convierta en un respetable hombre de negocios, pero éste, impaciente, y con tán solo diecisiete años de edad quiere algo más; seguir los pasos de su padre y ser uno más en los negocios de la familia.', '2016-06-14 20:21:52', NULL, 1, '8415729146', '8415729146', '978-8415729143', 'spanish', 'Mario Puzo', 'Zeta Bolsillo', 'https://www.amazon.es/Familia-Corleone-Rocabolsillo-Bestseller/dp/8415729146/', '9.45', 'https://images-na.ssl-images-amazon.com/images/I/51NNc%2BC3WcL._SX323_BO1,204,203,200_.jpg'),
(17, 'Cien años de soledad', 'cien-anos-de-soledad', 'Señalada como «catedral gótica del lenguaje», este clásico del siglo XX es el enorme y espléndido tapiz de la saga de la familia Buendía, en la mítica aldea de Macondo.\r\n\r\nUn referente imprescindible de la vida y la narrativa latinoamericana.\r\n\r\n«Muchos años después, frente al pelotón de fusilamiento, el coronel Aureliano Buendía había de recordar aquella tarde remota en que su padre lo llevó a conocer el hielo. Macondo era entonces una aldea de veinte casas de barro y cañabrava construidas a la orilla de un río de aguas diáfanas que se precipitaban por un lecho de piedras pulidas, blancas y enormes como huevos prehistóricos. El mundo era tan reciente, que muchas cosas carecían de nombre, y para mencionarlas había que señalarlas con el dedo.»\r\n\r\nCon estas palabras empieza la novela ya legendaria en los anales de la literatura universal, una de las aventuras literarias más fascinantes de nuestro siglo. Millones de ejemplares de Cien años de soledad leídos en todas las lenguas y el Premio Nobel de Literatura coronando una obra que se había abierto paso «boca a boca» -como gusta decir al escritor- son la más palpable demostración de que la aventura fabulosa de la familia Buendía-Iguarán, con sus milagros, fantasías, obsesiones, tragedias, incestos, adulterios, rebeldías, descubrimientos y condenas, representaba al mismo tiempo el mito y la historia, la tragedia y el amor del mundo entero.\r\n\r\nPablo Neruda dijo...\r\n\r\n«El Quijote de nuestro tiempo.»', '2016-06-14 20:23:53', '2016-06-15 07:12:27', 1, '8497592204', '8497592204', '978-8497592208', 'spanish', 'Gabriel Garcia Marquez', 'Debolsillo', 'https://www.amazon.es/soledad-CONTEMPORANEA-Gabriel-Garcia-Marquez/dp/8497592204/', '9.45', 'https://images-na.ssl-images-amazon.com/images/I/61DvgHwt0tL._SX325_BO1,204,203,200_.jpg'),
(18, 'El guardián entre el centeno', 'el-guardian-entre-el-centeno', 'Las peripecias del adolescente Holden Cauldfiel en una Nueva York que se recupera de la guerra influyeron en sucesivas generaciones de todo el mundo. En su confesión sincera y sin tapujos, muy lejos de la visión almibarada de la adolescencia que imperó hasta entonces, Holden nos desvela la realidad de un muchacho enfrentado al fracaso escolar, a las rígidas normas de una familia tradicional, a la experiencia de la sexualidad más allá del mero deseo.', '2016-06-14 20:27:06', '2016-06-15 07:12:56', 1, '8420674206', '8420674206', '978-8420674209', 'spanish', 'J D Salinger', 'Alianza', 'https://www.amazon.es/guardián-entre-centeno-Libro-Bolsillo/dp/8420674206/', '9.69', 'https://images-na.ssl-images-amazon.com/images/I/51uRrTIXtoL._SX327_BO1,204,203,200_.jpg'),
(19, 'El Gran Gatsby', 'el-gran-gatsby', 'Esta es la historia del millonario hecho a sí mismo, Jay Gatsby, a quien sólo le mueve una obsesión: recuperar un amor de juventud. Pero Daisy es hoy una muchacha que forma parte de una sociedad frívola y aburrida de sí misma, una criatura encantadora y también dañina. Un magnífico retrato de heroicidad en un mundo decadente.', '2016-06-14 20:28:33', '2016-06-15 07:11:33', 1, '8492892676', '8492892676', '978-8492892679', 'spanish', 'F. Scott Fidgerald', 'Jorge A. Mestas. Ediciones Escolares', 'https://www.amazon.es/El-Gran-Gatsby-Ed-Integra/dp/8492892676/', '4.70', 'https://images-na.ssl-images-amazon.com/images/I/51hGf2lzLDL._SX312_BO1,204,203,200_.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1464263099),
('m140209_132017_init', 1464263104),
('m140403_174025_create_account_table', 1464263104),
('m140504_113157_update_tables', 1464263105),
('m140504_130429_create_token_table', 1464263106),
('m140506_102106_rbac_init', 1465064338),
('m140830_171933_fix_ip_field', 1464263106),
('m140830_172703_change_account_table_name', 1464263106),
('m141222_110026_update_ip_field', 1464263106),
('m141222_135246_alter_username_length', 1464263107),
('m150614_103145_update_social_account_table', 1464263108),
('m150623_212711_fix_username_notnull', 1464263108);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`) VALUES
(1, 'Jorge', 'jorgepalleja@gmail.com', 'jorgepalleja@gmail.com', '9f26cde0a73432573bba91501baff099', 'Valencia', 'http://www.caralibro.xyz', 'Estudiante de DAW en el CEEDCV'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_account`
--

CREATE TABLE IF NOT EXISTS `social_account` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(4, 'jRIbM-t9xYzPFDYJ-7BhVjpEW0H1vocJ', 1465206734, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`) VALUES
(1, 'jorgepalleja', 'jorgepalleja@gmail.com', '$2y$10$UOrwAfKJiBjLA5wlevzUueX/OlPdZP48NidoZDEhKwKogKvAaHoye', 'pFf5FdU78DWLxXh_xBJAUm4GWCzlmxL5', 1464263599, NULL, NULL, '127.0.0.1', 1464263442, 1464263442, 0),
(2, 'moderador1', 'moderador1@caralibro.xyz', '$2y$10$PmW4XPHlg3aw2ftn9uJOFu4FOHFgGzyqAYN4u32iwdE7nyO.uyfzi', 'ngo1uq_cJwOYAKbd4_9fIPYIE-AOK8Wc', 1465205694, NULL, NULL, '127.0.0.1', 1465205694, 1465206709, 0),
(3, 'usuario1', 'usuario1@caralibro.xyz', '$2y$10$062jYq72wMrjBYjGXqOqD.Qy256jHAlQwdZfr4o0U/407i1cdlrOO', 'ERn82yML34-IXyttwdDPFyrrFIoLVz7x', 1465206653, NULL, NULL, '127.0.0.1', 1465206653, 1465206653, 0),
(4, 'usuario2', 'usuario2@caralibro.xyz', '$2y$10$3F22S2G1fS5OI/FIVg0khef6WjoCfews5cAssErcHFwwCFGNKTDr6', 'FQXu1fyc-onSelCVPqd3sXjBHrwPDw6m', 1465206748, NULL, NULL, '127.0.0.1', 1465206734, 1465206734, 0),
(5, 'usuario3', 'usuario3@caralibro.xyz', '$2y$10$5PmOl17baqI5tHu1X7NkfeWdUycv6SnptBB.6b2P5pziXqlV96Oo6', 'v_alBzs3kdG7htTtfoN2RkjVZheYmay4', 1465209495, NULL, NULL, '127.0.0.1', 1465209495, 1465209495, 0),
(6, 'usuario4', 'usuario4@caralibro.xyz', '$2y$10$BUmY.DEiOsyaMGCxCvrSzOb8kuLeqd9ATz7imF8UTK72Dubg1koVC', '5UpoI0BUUK-_wA0ru0nzSj-sjMsKIKrC', 1465209605, NULL, NULL, '127.0.0.1', 1465209605, 1465209605, 0),
(7, 'usuario5', 'usuario5@caralibro.xyz', '$2y$10$IMXvG/7Edi2FOUOswMo2F.oYS6wuGPRw4lDcXZz/p2PoxgfkJAsTe', 'MF7uaXz45-jGl77pvk_B0UIQ-ktvEGg3', 1465209827, NULL, NULL, '127.0.0.1', 1465209827, 1465209827, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_book_favorite`
--

CREATE TABLE IF NOT EXISTS `user_book_favorite` (
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `favorite` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_book_favorite`
--

INSERT INTO `user_book_favorite` (`user_id`, `book_id`, `favorite`, `timestamp_modified`) VALUES
(1, 1, 1, '2016-06-15 08:08:12'),
(1, 2, 1, '0000-00-00 00:00:00'),
(1, 3, 1, '2016-06-14 18:04:42'),
(1, 6, 1, '2016-06-14 20:35:26'),
(1, 13, 1, '2016-06-14 20:37:21'),
(1, 19, 0, '2016-06-15 08:50:13'),
(2, 1, 1, '0000-00-00 00:00:00'),
(3, 1, 1, '0000-00-00 00:00:00'),
(3, 4, 1, '2016-06-14 20:56:43'),
(3, 10, 1, '2016-06-14 20:55:13'),
(3, 16, 1, '2016-06-14 20:54:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_book_finished`
--

CREATE TABLE IF NOT EXISTS `user_book_finished` (
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `finished` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_book_finished`
--

INSERT INTO `user_book_finished` (`user_id`, `book_id`, `finished`, `timestamp_modified`) VALUES
(1, 1, 1, '2016-06-14 20:53:33'),
(1, 6, 1, '2016-06-14 20:53:49'),
(1, 13, 0, '2016-06-14 20:53:24'),
(1, 18, 1, '2016-06-16 09:31:46'),
(1, 19, 1, '2016-06-14 20:53:11'),
(3, 1, 1, '2016-06-14 20:55:24'),
(3, 2, 1, '2016-06-14 20:55:16'),
(3, 10, 1, '2016-06-14 20:55:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_book_pending`
--

CREATE TABLE IF NOT EXISTS `user_book_pending` (
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_book_pending`
--

INSERT INTO `user_book_pending` (`user_id`, `book_id`, `pending`, `timestamp_modified`) VALUES
(1, 1, 0, '2016-06-15 08:08:12'),
(1, 3, 1, '2016-06-14 18:05:14'),
(1, 4, 1, '2016-06-14 20:53:45'),
(1, 7, 1, '2016-06-14 20:35:28'),
(1, 12, 1, '2016-06-14 20:35:32'),
(1, 13, 1, '2016-06-14 20:35:36'),
(1, 18, 1, '2016-06-14 20:35:39'),
(1, 19, 0, '2016-06-14 20:44:32'),
(3, 2, 1, '2016-06-14 20:55:20'),
(3, 11, 1, '2016-06-14 20:55:30');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
 ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indices de la tabla `auth_item`
--
ALTER TABLE `auth_item`
 ADD PRIMARY KEY (`name`), ADD KEY `rule_name` (`rule_name`), ADD KEY `idx-auth_item-type` (`type`);

--
-- Indices de la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Indices de la tabla `auth_rule`
--
ALTER TABLE `auth_rule`
 ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `book`
--
ALTER TABLE `book`
 ADD PRIMARY KEY (`idbook`), ADD UNIQUE KEY `asin_UNIQUE` (`asin`);

--
-- Indices de la tabla `migration`
--
ALTER TABLE `migration`
 ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `profile`
--
ALTER TABLE `profile`
 ADD PRIMARY KEY (`user_id`);

--
-- Indices de la tabla `social_account`
--
ALTER TABLE `social_account`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `account_unique` (`provider`,`client_id`), ADD UNIQUE KEY `account_unique_code` (`code`), ADD KEY `fk_user_account` (`user_id`);

--
-- Indices de la tabla `token`
--
ALTER TABLE `token`
 ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_unique_email` (`email`), ADD UNIQUE KEY `user_unique_username` (`username`);

--
-- Indices de la tabla `user_book_favorite`
--
ALTER TABLE `user_book_favorite`
 ADD PRIMARY KEY (`user_id`,`book_id`), ADD KEY `fk_user_book_favorite_book_idx` (`book_id`) USING BTREE, ADD KEY `fk_user_book_favorite_user_idx` (`user_id`);

--
-- Indices de la tabla `user_book_finished`
--
ALTER TABLE `user_book_finished`
 ADD PRIMARY KEY (`user_id`,`book_id`), ADD KEY `fk_finished_user_idx` (`user_id`) USING BTREE, ADD KEY `fk_finished_book_idx` (`book_id`) USING BTREE;

--
-- Indices de la tabla `user_book_pending`
--
ALTER TABLE `user_book_pending`
 ADD PRIMARY KEY (`user_id`,`book_id`), ADD UNIQUE KEY `fk_pending_user_idx` (`user_id`,`book_id`), ADD KEY `fk_pending_book_idx` (`book_id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `book`
--
ALTER TABLE `book`
MODIFY `idbook` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `social_account`
--
ALTER TABLE `social_account`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item`
--
ALTER TABLE `auth_item`
ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `profile`
--
ALTER TABLE `profile`
ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `social_account`
--
ALTER TABLE `social_account`
ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `token`
--
ALTER TABLE `token`
ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user_book_favorite`
--
ALTER TABLE `user_book_favorite`
ADD CONSTRAINT `fk_favorite_book` FOREIGN KEY (`book_id`) REFERENCES `book` (`idbook`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_favorite_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_book_finished`
--
ALTER TABLE `user_book_finished`
ADD CONSTRAINT `fk_finished_book` FOREIGN KEY (`book_id`) REFERENCES `book` (`idbook`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_finished_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_book_pending`
--
ALTER TABLE `user_book_pending`
ADD CONSTRAINT `fk_pending_book` FOREIGN KEY (`book_id`) REFERENCES `book` (`idbook`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_pending_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
