<?php

$params = require(__DIR__ . '/params.php');

if (YII_ENV_DEV) {
    // This mailer is useful when developing at localhost
    // It stores the sent emails in caralibro/runtime/mail
    $mailer = [
        'class' => 'yii\swiftmailer\Mailer',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => true,
    ];
} else {
    // This configuration is for production
    // It uses the gmail smtp
    $mailer = [
        'class' => 'yii\swiftmailer\Mailer',
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.gmail.com',
            'username' => 'caralibroceedcv',
            'password' => 'Proyecto2016',
            'port' => '587',
            'encryption' => 'tls',
    ]];
}

$config = [
    'id' => 'basic',
    'name' => 'Caralibro',
    'basePath' => dirname(__DIR__),
    'language' => 'es',
    'bootstrap' => ['log'],
    'defaultRoute' => 'book',
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'admins' => ['jorgepalleja'],
//            'enableRegistration' => false, // This would disable the register page
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
    ],
    'components' => [
        /* Here we can configure identity providers for the app 
         * using the api key for each one
         */

//        'authClientCollection' => [
//            'class' => yii\authclient\Collection::className(),
//            'clients' => [
//                'facebook' => [
//                    'class' => 'dektrium\user\clients\Facebook',
//                    'clientId' => 'CLIENT_ID',
//                    'clientSecret' => 'CLIENT_SECRET',
//                ],
//                'twitter' => [
//                    'class' => 'dektrium\user\clients\Twitter',
//                    'consumerKey' => 'CONSUMER_KEY',
//                    'consumerSecret' => 'CONSUMER_SECRET',
//                ],
//                'google' => [
//                    'class' => 'dektrium\user\clients\Google',
//                    'clientId' => 'CLIENT_ID',
//                    'clientSecret' => 'CLIENT_SECRET',
//                ],
//            ],
//        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['usuario'],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Uq5MzC_PPysnoApr8CU2m4nuDX7Ey7P5',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        // Yii2 original user module (we changed it for dektrium\user\Module)
//        'user' => [
//            'identityClass' => 'app\models\User',
//            'enableAutoLogin' => true,
//        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => $mailer,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
//                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                'libro/<id:\d+>/<slug>' => 'book/view',
                'libros' => 'book/index',
                'libros/favoritos' => 'book/favorites',
                'libros/pendientes' => 'book/pending',
                'libros/leidos' => 'book/finished',
                'libros/buscar' => 'book/search',
//                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
//                'usuario/<action:\w+>' => 'user/<action>',
//                'usuario/<action:\w+>' => 'user/<action>',
//                'usuario/<id:\d+>/<username>' => 'user/<id>/<username>',
                'user' => 'user/profile',
                'users' => 'user/profile/usuarios',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ]
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
