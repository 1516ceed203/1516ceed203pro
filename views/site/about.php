<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Acerca de Caralibro';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div>
        <p>Proyecto realizado por Jorge Francisco Palleja Solanes</p>
        <p><a href='mailto:jorgepalleja@gmail.com'>jorgepalleja@gmail.com</a></p>
    </div>
    <br>
    

    <div class="alert alert-success">

        <strong>Usuarios ya registrados</strong> (password: ceedcv)
    </div>
    <div>
        <p><strong>Admin:</strong> jorgepalleja</p>
        <p><strong>Moderador:</strong> moderador1</p>
        <p><strong>Usuarios:</strong> usuario1, usuario2, usuario3, usuario4, usuario5</p>
    </div>
    
    <br>
    <div class="alert alert-info">
        <p>
            Swiftmailer guarda los emails enviados para confirmar nuevos registros en <strong>caralibro/runtime/mail</strong>
        </p>
    </div>

    <!--<code><?= __FILE__ ?></code>-->
</div>
