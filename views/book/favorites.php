
<?php

$this->title = 'Mis favoritos';
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('/book/_menu');

$models = $dataProvider->getModels();

if (!$models) {
    echo 'No tienes ningún libro favorito todavía';
} else {
  
    echo $this->render('/book/_list', ['models' => $models, 'dataProvider' => $dataProvider]);
}
?>



