<div class="row">
    <div class="col-sm-12">        

        <?php

        use yii\helpers\Html;
        use yii\helpers\Url;

foreach ($models as $model) {
            //    var_dump($model);
            ?>
            <div class="col-sm-4 col-md-3 col-lg-3 col-xs-6" >
                <div class = "well well-sm" >
                    <div class="item-image">
                        <a href = "
                        <?php
                        echo yii\helpers\Url::to(['/book/view', 'id' => $model['idbook'], 'slug' => $model->slug])
                        ?>">

                            <?php
                            echo Html::img(Url::to(Yii::getAlias('@web')."/img/covers/" . $model['idbook'] . ".jpg", true), ['class' => 'img-responsive',
                                'alt' => Html::encode($model->title),
                                'title' => Html::encode($model->title),
                                'style' => 'display: block; width: 100%']);
                            ?>
                    </div>
                    <div class="item-content hidden-xs visible-sm visible-md visible-lg">
                        <?php echo $model->title; ?>
                    </div>

                    </a>

                </div>
            </div>
            <?php
        }
        ?>
    </div>


</div>
<?php
echo \yii\widgets\LinkPager::widget([
    'pagination' => $dataProvider->pagination,
]);
?>
