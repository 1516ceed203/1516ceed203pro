<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \dektrium\user\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Libros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .red{
        color: red;
    }
    .grey{
        color: grey;
    }
    .green{
        color: green;
    }
    .blue{
        color: blue;
    }
    .grey:hover {
        color: red;
    }
</style>

<script>
    function sendAjax(book_id, color, item) {
        var ajaxUrl = "<?php echo Yii::$app->getUrlManager()->createUrl('book/ajax'); ?>";
        var action = 'like';
        var title = '';

        switch (item.id) {
            case 'heart':
                action = 'favorite';
//                alert('corazon');
                break;
            case 'clock':
                action = 'pending';
//                alert('reloj');
                break;
            case 'eye':
                action = 'finished';
//                alert('ojo');
                break;
        }
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->getUrlManager()->createUrl('book/ajax'); ?>",
            data: {id: book_id, action: action},
            success: function () {

                if (item.id === 'heart') {
                    $('#heart').toggleClass('glyphicon-heart-empty glyphicon-heart');
                }
                $(item).toggleClass('grey ' + color);
            },
            error: function () {
                var r = confirm("Necesitas haber iniciado sesión");
                if (r == true) {
                    window.location.replace('<?php echo Yii::$app->getUrlManager()->createUrl('user/login'); ?>')
                }

            }
        })
    }

</script>
<?= $this->render('/book/_menu') ?>
<?php
$favorite_class = "grey glyphicon glyphicon-heart-empty";
$pending_class = "grey glyphicon glyphicon-time";
$finished_class = "grey glyphicon glyphicon-eye-open";

if (!Yii::$app->user->isGuest) {
    $userId = Yii::$app->user->identity->id;
    $favorite = User::inList($model->idbook, $userId, 'favorite');
    $pending = User::inList($model->idbook, $userId, 'pending');
    $finished = User::inList($model->idbook, $userId, 'finished');
    if ($favorite) {
        $favorite_class = "red glyphicon glyphicon-heart";
    }
    if ($pending) {
        $pending_class = "blue glyphicon glyphicon-time";
    }
    if ($finished) {
        $finished_class = "green glyphicon glyphicon-eye-open";
    }
}
?>
<div class="book-view">

    <h1>
        <span id='heart' title='Favorito' style="font-size: x-large" class="<?= $favorite_class ?>" 
              onclick="sendAjax(<?= $model->idbook ?>, 'red', this)" onhover='color=red'></span>
        <span id='clock' title='Pendiente' style="font-size: x-large" class="<?= $pending_class ?>" 
              onclick="sendAjax(<?= $model->idbook ?>, 'blue', this)"></span>
        <span id='eye' title='Leído' style="font-size: x-large" class="<?= $finished_class ?>" 
              onclick="sendAjax(<?= $model->idbook ?>, 'green', this)"></span>
              <?= Html::encode($this->title) ?>
    </h1>

    <p>

        <?php
        if (!Yii::$app->user->isGuest && Yii::$app->user->can('moderador')) {
            echo Html::a('Update', ['update', 'id' => $model->idbook], ['class' => 'btn btn-primary']);
            echo ' ';

            echo Html::a('Delete', ['delete', 'id' => $model->idbook], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);

            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'idbook',
                    'title',
                    'description:ntext',
                    'date_entered',
                    'date_updated',
                    'working',
                    'asin',
                    'isbn',
                    'isbn13',
                    'language',
                    'author',
                    'publisher',
                    'amazon_url:url',
                    'amazon_price_eur',
                    'amazon_cover_url:url',
                    ['label' => 'Comprar',
                        'format' => 'raw',
                        'value' => Html::a("Amazon ($model->amazon_price_eur €)", $model->amazon_url, ['class' => 'btn btn-warning', 'target' => '_blank'])],
                ],
            ]);
        } else {
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'title',
                    'author',
                    'description:ntext',
                    'publisher',
//                    'amazon_url:url',
//                    'amazon_price_eur',
                    ['label' => 'Comprar',
                        'format' => 'raw',
                        'value' => Html::a("Amazon ($model->amazon_price_eur €)", $model->amazon_url, ['class' => 'btn btn-warning', 'target' => '_blank'])],
                ],
            ]);
        }
        ?>
    </p>



</div>
<div id="disqus_thread"></div>
<script>
    /**
     * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
     */

    var disqus_config = function () {
//     this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = '<?= $model->idbook ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };

    (function () { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');

        s.src = '//caralibro2016.disqus.com/embed.js';

        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57471715a5295955"></script>



