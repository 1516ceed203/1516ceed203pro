<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buscador';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('/book/_menu') ?>

<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    // echo $this->render('_search', ['model' => $searchModel]);  

    if (!Yii::$app->user->isGuest && Yii::$app->user->can('moderador')) {
        ?>

        <div>
            <form action='' method='get'>
                <?= Html::a('Nuevo libro', ['create'], ['class' => 'btn btn-success']) ?>
                <input type="hidden" value='0' name="BookSearch[working]"/>
                <input type="submit" value="Ver borrados" class="btn btn-danger"/>
            </form>
            <br>
        </div>
    <?php }
    ?>




    <!--    This div forces the inside content to be responsive-->
    <div class="table-responsive">
        <?php
        $columns = [
//                ['class' => 'yii\grid\SerialColumn'], // This would display a results counter
            //'idbook',
            [
                'attribute' => 'Portada',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::a(Html::img(Url::to(Yii::getAlias('@web')."/img/covers/" . $data['idbook'] . ".jpg", true), ['width' => '150px']), ['book/view/', 'id' => $data['idbook'], 'slug' => $data['slug']]);
                },
                    ],
                    'title',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                        'value' => function ($data) {
                            $str = $data['description'];
                            $str_xs = $str_sm = $str_md = $str_lg = '';
                            if (strlen($str) > 150) {
                                $str_xs = $str_sm = $str_md = $str_lg = substr($str, 0, 150) . '...';
                            } else {
                                $str_xs = $str_sm = $str_md = $str_lg = $str;
                            }
                            if (strlen($str) > 300) {
                                $str_sm = $str_md = $str_lg = substr($str, 0, 300) . '...';
                            } else {
                                $str_sm = $str_md = $str_lg = $str;
                            }
                            if (strlen($str) >= 900) {
                                $str_md = $str_lg = substr($str, 0, 900) . '...';
                            } else {
                                $str_md = $str_lg = $str;
                            }
                            $description_html = ""
                                    . "<span class='visible-xs'>$str_xs</span>"
                                    . "<span class='visible-sm'>$str_sm</span>"
                                    . "<span class='visible-md'>$str_md</span>"
                                    . "<span class='visible-lg'>$str_lg</span>";
                            return $description_html;
                        },
                    ],
//                                    'description:ntext',
                    'author',
                ];

                // The moderator has more rows with shortcuts
                if (!Yii::$app->user->isGuest && Yii::$app->user->can('moderador')) {
                    // We add a working
                    $columns[] = 'working';
                    $columns[] = ['class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['book/view/', 'id' => $model['idbook'], 'slug' => $model['slug']]);
                            }
                                ],
                            ];

                            /*
                              $columns[] = 'date_entered';
                              $columns[] = 'date_updated';
                             * 
                              Other possible rows are:
                              //'date_entered',
                              //'date_updated',
                              // 'working',
                              // 'asin',
                              // 'isbn',
                              // 'isbn13',
                              // 'language',
                             */
                        }
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => $columns,
                        ]);
                        ?>
    </div>
</div>