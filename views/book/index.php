
<?php

$this->title = 'Novedades';
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('/book/_menu');

$models = $dataProvider->getModels();

if (!$models) {
    echo 'No hay libros en la base de datos';
} else {
  
    echo $this->render('/book/_list', ['models' => $models, 'dataProvider' => $dataProvider]);
}
?>



