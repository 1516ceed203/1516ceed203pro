
<?php

$this->title = 'Libros leídos';
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('/book/_menu');

$models = $dataProvider->getModels();

if (!$models) {
    echo 'No has añadido ningún libro a tu lista de libros leídos';
} else {
  
    echo $this->render('/book/_list', ['models' => $models, 'dataProvider' => $dataProvider]);
}
?>



