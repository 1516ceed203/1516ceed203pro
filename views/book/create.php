<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = 'Crear libro';
$this->params['breadcrumbs'][] = ['label' => 'Libros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?=
Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
        'style' => 'margin-bottom: 15px',
//        'activateItems'=>'true',
//    'activateParents'=>'true',
    ],
    'items' => [
        ['label' => 'Entrada manual', 'url' => ['/book/create']],
//        ['label' => 'Entrada automatica'], // Usando Api de amazon para auto rellenar
    ],
])
?>
<div class="book-create">

    <h1><?= Html::encode($this->title) ?></h1>

<?=
$this->render('_form', [
    'model' => $model,
])
?>

</div>
