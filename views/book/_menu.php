<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\Nav;
?>

<?=

Nav::widget([
    'options' => [
        'class' => 'nav-tabs',
        'style' => 'margin-bottom: 15px',
//        'activateItems'=>'true',
//    'activateParents'=>'true',
    ],
    'items' => [
        [
            'label' => 'Novedades',
            'url' => ['/book/index'],
        ],
        [
            'label' => 'Buscar libro',
            'url' => ['/book/search'],
        ],
        [
            'label' => 'Mis favoritos',
            'url' => ['/book/favorites'],
        ],
        [
            'label' => 'Pendientes',
            'url' => ['/book/pending'],
        ],
        [
            'label' => 'Leídos',
            'url' => ['/book/finished'],
        ],
        [
            'visible' => (!Yii::$app->user->isGuest && Yii::$app->user->can('moderador')),
            'label' => 'Moderador',
            'items' => [
                [
                    'label' => 'Crear libro',
                    'url' => ['/book/create'],
                ],
            ],
        ],
        [
            'visible' => (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin),
            'label' => 'Administrador',
            'items' => [

                [

                    'label' => 'Ver',
                    'url' => ['/user/admin/'],
                    'items' => [
                        [
                            'label' => Yii::t('user', 'Users'),
                            'url' => ['/user/admin/index'],
                        ],
                        [
                            'label' => Yii::t('user', 'Roles'),
                            'url' => ['/rbac/role/index'],
                            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                        ],
                        [
                            'label' => Yii::t('user', 'Permissions'),
                            'url' => ['/rbac/permission/index'],
                            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                        ],
                    ],
                ],
                [

                    'label' => Yii::t('user', 'Create'),
                    'url' => ['/user/admin/'],
                    'items' => [
                        [
                            'label' => Yii::t('user', 'New user'),
                            'url' => ['/user/admin/create'],
                        ],
                        [
                            'label' => Yii::t('user', 'New role'),
                            'url' => ['/rbac/role/create'],
                            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                        ],
                        [
                            'label' => Yii::t('user', 'New permission'),
                            'url' => ['/rbac/permission/create'],
                            'visible' => isset(Yii::$app->extensions['dektrium/yii2-rbac']),
                        ],
                    ],
                ],
            ],
        ],
    ],
])
?>

