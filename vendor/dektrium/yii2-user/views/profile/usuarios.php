
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<form id="w0" action="" method="get">

    <div class="form-group field-usersearch-username">
        <label class="control-label" for="usersearch-username">Nombre de usuario</label>
        <input type="text" id="usersearch-username" class="form-control" name="UserSearch[username]" placeholder="username">

        <div class="help-block"></div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-success">Buscar</button>    </div>
</form>
<div class="col-sm-12">
    <div class="row" style="flex-direction: column">

         <?php
         $models = $dataProvider->getModels();
         foreach ($models as $model) {
             //    var_dump($model);
             if ($model->profile) {
                 ?>
                 <div class="col-sm-4 col-md-3 col-lg-3 col-xs-6" >
                    <div class = "well well-sm" >
                        <a href = "
        <?php
        echo yii\helpers\Url::to(['/user/profile/show', 'id' => $model['id'], 'username' => $model->username])
        ?>">

                           <?php
                           echo Html::img($model->profile->getAvatarUrl(),  ['class' => 'img-responsive',
                                'alt' => Html::encode($model->username),
                                'title' => Html::encode($model->username),
                                'style' => 'display: block; width: 100%']);
                           ?>
                            <div class="item-content">
                            <?php
                            if ($model->profile->name) {
                                echo $model->profile->name;
                            }
                            echo ' (' . Html::encode($model->username) . ')';
                            ?>
                            </div>

                        </a>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>

<?php
echo \yii\widgets\LinkPager::widget([
    'pagination' => $dataProvider->pagination,
]);
?>
</div>


