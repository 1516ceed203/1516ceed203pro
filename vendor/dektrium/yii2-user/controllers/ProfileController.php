<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dektrium\user\controllers;

use dektrium\user\Finder;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
/**
 * ProfileController shows users profiles.
 *
 * @property \dektrium\user\Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class ProfileController extends Controller {

    /** @var Finder */
    protected $finder;

    /**
     * @param string           $id
     * @param \yii\base\Module $module
     * @param Finder           $finder
     * @param array            $config
     */
    public function __construct($id, $module, Finder $finder, $config = []) {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /** @inheritdoc */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['index'], 'roles' => ['@']],
                    ['allow' => true, 'actions' => ['show'], 'roles' => ['?', '@']],
                    ['allow' => true, 'actions' => ['usuarios'], 'roles' => ['?', '@']],
                ],
            ],
        ];
    }

    /**
     * Redirects to current user's profile.
     *
     * @return \yii\web\Response
     */
    public function actionIndex() {
//        echo Yii::$app->user->identity->username; die();
//        return $this->redirect(['show', 'id' => Yii::$app->user->getId(), 'username'=>Yii::$app->user->identity->username]);
//        return $this->redirect(['show', 'id' => Yii::$app->user->getId(), 'username'=>Yii::$app->user->identity->username]);
        return $this->redirect(['show', 'id' => Yii::$app->user->getId(), 'username' => Yii::$app->user->identity->username]);
    }

    /**
     * Shows user's profile.
     *
     * @param int $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionShow($id) {
        $profile = $this->finder->findProfileById($id);

        if ($profile === null) {
            throw new NotFoundHttpException();
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Book::find()
                    ->leftJoin('user_book_favorite', '`book`.`idbook` = `user_book_favorite`.`book_id`')
                    ->where(['user_book_favorite.user_id' => $id,
                        'user_book_favorite.favorite' => 1,
                        'book.working' => 1])
                    ->orderBy(['user_book_favorite.timestamp_modified' => SORT_DESC])
            , 'pagination' => ['pageSize' => 4]
        ]);


        return $this->render('show', [
                    'profile' => $profile, 'dataProvider' => $dataProvider,
        ]);
    }

//    public function actionShow($username)
//    {
//        $profile = $this->finder->findProfileByUsername($username);
//
//        if ($profile === null) {
//            throw new NotFoundHttpException();
//        }
//
//        return $this->render('show', [
//            'profile' => $profile,
//        ]);
//    }

    /**
     * Shows user's profile.
     *
     * @param int $id
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUsuarios() {
        $searchModel = Yii::createObject(\dektrium\user\models\UserSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize=4;
//        $dataProvider->query->where('user.id = 1');
//        $dataProvider->query->join('LEFT JOIN', 'profile', 'user.id = profile.user_id');
//        $dataProvider = new \yii\data\ActiveDataProvider([
//'query' => \dektrium\user\models\User::find(),
//'pagination' => ['pageSize'=>3]
//]);
////        return $this->render('usuarios');
//        return $this->render('usuarios', [
//                    'dataProvider' => $dataProvider,
//                    'searchModel' => $searchModel,
//        ]);
        
//        $dataProvider = new ActiveDataProvider([
//            'query' => \dektrium\user\models\User::find(),
//            'pagination' => ['pageSize' => 5]
//        ]);

        return $this->render('usuarios', 
                ['dataProvider' => $dataProvider,]);
    }
    
    public function userPending($id) {
        $userId = Yii::$app->user->identity->id;

        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Book::find()
                    ->leftJoin('user_book_pending', '`book`.`idbook` = `user_book_pending`.`book_id`')
                    ->where(['user_book_pending.user_id' => $userId,
                        'user_book_pending.pending' => 1,
                        'book.working' => 1])
                    ->orderBy(['user_book_pending.timestamp_modified' => SORT_DESC])
            , 'pagination' => ['pageSize' => 2]
        ]);

        return $this->render('pending', ['dataProvider' => $dataProvider,]);
    }

}
